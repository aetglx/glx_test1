﻿using System;
using GLX_POC.MainSetup;
using GLX_POC.PageObjects;
using NUnit.Framework;
using TechTalk.SpecFlow;


namespace GLX_POC
{
    /// <summary>
    /// The MCALoginSteps class.
    /// Contains step definitions of Login feature.
    /// </summary>
    [Binding]
    public class MCALoginSteps
    {
        LoginPage loginPage = new LoginPage();
        HomePage homePage = new HomePage();

        [BeforeScenario]
        public void BeforeScenario()
        {
            DriverSetup.SetDriver();
        }

        [Given(@"the management console app a logged in  (.*)and (.*)")]
        public void GivenTheManagementConsoleAppALoggedIn(string username, string password)
        {
            loginPage.Login(username, password);
        }

        [Then(@"user  navigates to home screen (.*)")]
        public void ThenUserNavigatesToHomeScreen(string profileName)
        {
            Assert.IsTrue(homePage.getProfileName().Equals(profileName));
        }

        [Given(@"the management console app invalid credentials (.*) and (.*)")]
        public void GivenTheManagementConsoleAppInvalidCredentials(string username, string password)
        {
            loginPage.Login(username, password);
        }

        [Then(@"returns management console login error")]
        public void ThenReturnsManagementConsoleLoginError()
        {
            Assert.IsTrue(homePage.IdentifyLoginError().Text.Equals("Employee not found."));
        }

        [Then(@"Log out from MCA")]
        public void ThenLogOutFromMCA()
        {
            homePage.ClickProfileDropdown();
            homePage.ClickSignOut();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            DriverSetup.GetDriver().Quit();
        }
    }
}
