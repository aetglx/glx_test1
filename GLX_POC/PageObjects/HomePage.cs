﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GLX_POC.MainSetup;
using NUnit.Framework;
using OpenQA.Selenium;

namespace GLX_POC.PageObjects
{
    /// <summary>
    /// The HomePage class.
    /// Contains home page pageObjects.
    /// </summary>
    class HomePage
    {
        private By dd_ProfileDropdown = By.XPath("//a/span[contains(text(),'Awesom-o 2000')]");
        private By lnk_Logout = By.XPath("//a[contains(text(),'Logout')]");
        private By lbl_loginError = By.XPath("//div[contains(text(),'Employee not found.')]");

        /// <summary>
        /// Retrieves the profile dropdown element.
        /// </summary>
        /// <returns>Profile dropdown element</returns>
        private IWebElement IdentifyProfileDropdown()
        {
            return CommonUtils.IsElementVisible(dd_ProfileDropdown);
        }

        /// <summary>
        /// Retrieves the sign out element.
        /// </summary>
        /// <returns>Sign out element</returns>
        private IWebElement IdentifySignOut()
        {
            return CommonUtils.IsElementVisible(lnk_Logout);
        }

        /// <summary>
        /// Retrieves the login error element.
        /// </summary>
        /// <returns>Login error element</returns>
        public IWebElement IdentifyLoginError()
        {
            return CommonUtils.IsElementVisible(lbl_loginError);
        }

        /// <summary>
        /// Extracts the profile name.
        /// </summary>
        /// <returns>The profile name</returns>
        public String getProfileName()
        {
            return IdentifyProfileDropdown().Text.Trim();
        }

        /// <summary>
        /// Clicks the profile dropdown.
        /// </summary>
        public void ClickProfileDropdown()
        {
            this.IdentifyProfileDropdown().Click();
        }

        /// <summary>
        /// Clicks the sign out.
        /// </summary>
        public void ClickSignOut()
        {
            this.IdentifySignOut().Click();
        }
    }
}

