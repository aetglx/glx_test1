﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using GLX_POC.MainSetup;
using OpenQA.Selenium;

namespace GLX_POC.PageObjects
{
    /// <summary>
    /// The LoginPage class.
    /// Contains login page pageObjects.
    /// </summary>
    class LoginPage
    {
        private By tf_username = By.XPath("//input[@name='username']");
        private By tf_password = By.XPath("//input[@name='password']");
        private By btn_signIn = By.XPath("//button[contains(text(),'Sign In')]");

        /// <summary>
        /// Retrieves the username element.
        /// </summary>
        /// <returns>Username element</returns>
        private IWebElement IdentifyUsername()
        {
            return CommonUtils.IsElementVisible(tf_username);
        }

        /// <summary>
        /// Retrieves the password element.
        /// </summary>
        /// <returns>Password element</returns>
        private IWebElement IdentifyPassword()
        {
            return CommonUtils.IsElementVisible(tf_password);
        }

        /// <summary>
        /// Retrieves the sign in element.
        /// </summary>
        /// <returns>Sign in element</returns>
        private IWebElement IdentifySignIn()
        {
            return CommonUtils.IsElementVisible(btn_signIn);
        }

        /// <summary>
        /// Enters the username.
        /// </summary>
        /// <param name="username"> Login username.</param>
        public void EnterUsername(String username)
        {
            this.IdentifyUsername().SendKeys(username.Trim());
        }

        /// <summary>
        /// Enters the password.
        /// </summary>
        /// <param name="password">Login password.</param>
        public void EnterPassword(String password)
        {
            this.IdentifyPassword().SendKeys(password.Trim());
        }

        /// <summary>
        /// Clicks the sign in.
        /// </summary>
        public void ClickSignIn()
        {
            this.IdentifySignIn().Click();
        }

        /// <summary>
        /// Logins the specified account.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        public void Login(String username, String password)
        {
            this.EnterUsername(username);
            this.EnterPassword(password);
            this.ClickSignIn();
        }

    }
}
