﻿Feature: MCALogin
	
@smoke
Scenario Outline: Valid Login
		Given the management console app a logged in  <username> and <password> 
		Then user  navigates to home screen <profileName>
		Then Log out from MCA

Examples:
    | username                     | password | profileName   |
    | glxautomator1@guestlogix.com | P@ssword | Awesom-o 2000 |


Scenario Outline: Invalid Login
	Given the management console app invalid credentials <username> and <password>
	Then returns management console login error
	
Examples:
    | username             | password |
    | aetqatest4@gmail.com | Password |


		
		
