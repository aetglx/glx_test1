﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;

namespace GLX_POC.MainSetup
{
    /// <summary>
    /// The Driver Setup class.
    /// Contains functions related to Chrome driver.
    /// </summary>
    class DriverSetup
    {
        static IWebDriver driver;

        /// <summary>
        /// Sets the driver.
        /// </summary>
        /// <returns></returns>
        public static IWebDriver  SetDriver()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://glx-management-console-test.s3-website.ca-central-1.amazonaws.com");
            driver.Navigate().Refresh();
            driver.Manage().Window.Maximize();

            return driver;
        }

        /// <summary>
        /// Gets the driver.
        /// </summary>
        /// <returns></returns>
        public static IWebDriver GetDriver()
        {
           return driver;
        }

        /// <summary>
        /// Teardowns this instance.
        /// </summary>
        public void Teardown()
        {
            driver.Quit();
        }
    }
}
