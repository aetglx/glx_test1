﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace GLX_POC.MainSetup
{
    /// <summary>
    /// The CommonUtils class.
    /// Contains commonly used utility methods.
    /// </summary>
    class CommonUtils
    {
        /// <summary>
        /// This method returns the element by waiting till its visible and enabled.
        /// </summary>
        /// <param name="by">Element locator</param>
        /// <returns>The element requested</returns>
        /// <remarks>
        /// Wait has a max limit of 15 seconds.
        /// </remarks>
        public static IWebElement IsElementVisible(By by)
        {
            try
            {
                IWebDriver _driver = DriverSetup.GetDriver();
                var wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(15));
                IWebElement iwebElement = wait.Until(x => x.FindElement(by));
                if (iwebElement.Displayed && iwebElement.Enabled)
                {
                    return iwebElement;
                }
                else
                {
                    return null;
                }
            }
            catch (NoSuchElementException ex)
            {
                Console.Out.WriteLine(ex.Message);
                return null;
            }
        }



    }
}
